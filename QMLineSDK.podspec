#
#  Be sure to run `pod spec lint QMLineSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "QMLineSDK"
  spec.version      = "4.0.0"
  spec.summary      = "QMlineSDK SDK"

  spec.description  = "iOS Frameworks for Customer Service"

  spec.homepage     = "https://github.com/7moor-tech/QMLineSDK"

  spec.license      = "MIT"

  spec.author       = { "ZCZ" => "942914231@qq.com" }

  spec.platform     = :ios

  spec.ios.deployment_target = "9.0"


  spec.source       = { :git => "https://github.com/7moor-tech/QMLineSDK.git", :tag => spec.version}


  spec.vendored_frameworks  = "QMLineSDK.framework"
  spec.pod_target_xcconfig = {'VALID_ARCHS'=>'armv7 x86_64 arm64'}

  spec.requires_arc = true
  spec.dependency 'Qiniu', '~> 8.4.0'
  spec.dependency 'FMDB', '~> 2.7.5'
  spec.dependency 'SocketRocket', '~> 0.5.1'

end
